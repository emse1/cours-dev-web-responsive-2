# Frontend Web Development

## Web dev for mobiles - Part 2

Quentin Richaud

qrichaud.pro@gmail.com

---

# How to access your development webpage from your smartphone

- Be on the same local network (either Home Wifi, or being on your smartphone Wifi Hotspot also works)
- Determine the IP adress of your laptop (linux users : `ìpconfig`)
- Access the IP adress (instead of `localhost` + development server port from your smartphone web browser). Example : `192.168.1.170:8000`

---

# Using Web Browser Geolocation API

Documentation of the API here : https://developer.mozilla.org/en-US/docs/Web/API/Geolocation_API

See the demo code

---

# Using the browser MediaDevice API

See the demo code

---

# Exercise

Use the Geolocation API and a public web service (Geonames) to display the current adress of the user.

