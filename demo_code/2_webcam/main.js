let canvas = document.querySelector("#canvas");
let video = document.querySelector("video");

document.querySelector(".start-webcam-btn").addEventListener("click", () => {
    navigator.mediaDevices.getUserMedia({
        audio: true,
        video: true
    })
    .then(stream => {
        video.srcObject = stream;
        video.onloadedmetadata = () => {
          video.play();
        };
    });
});





document.querySelector(".capture-img-btn").addEventListener("click", () => {
    /* Draw the image of the hidden canvas */
    canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);

    /* Create a download from the canvas' image */
    var link = document.createElement('a');
    link.download = 'screenshot.png';
    link.href = canvas.toDataURL('image/png')
    link.click(); 
});