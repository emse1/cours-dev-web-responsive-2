document.querySelector(".find-position-btn").addEventListener("click", () => {
    
 

    navigator.geolocation.getCurrentPosition((position) => {
        /* Sucess callback */
        console.log(position);
        displaySuccess(`You are located here : lat ${position.coords.latitude}, lng ${position.coords.longitude}`);
    }, (positionError) => {
        /* Error callback */
        console.log(positionError);
        displayError("We could not find your position");
    }, {
        /* Options */
        enableHighAccuracy: true
    });
});



function displaySuccess(text) {
    _display(text, "success");
}

function displayError(text) {
    _display(text, "error");
}

function _display(text, status) {
    Array.from(document.querySelectorAll(`.content .error`)).forEach(e => e.remove());
    Array.from(document.querySelectorAll(`.content .success`)).forEach(e => e.remove());
    document.querySelector(".content").insertAdjacentHTML("beforeend", `<p class="${status}">${text}</p>`);
}