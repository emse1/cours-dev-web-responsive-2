
We will make a webpage that gives us the current adress we're geolocated at.

For this we need to use the browser geolocation API to get our GPS coordinates.

Then we will use the free webservice Geonames, and more specifically these 2 services :

- `http://api.geonames.org/countrySubdivisionJSON?lat=<lat>&lng=<lng>&level=5&username=<username>`

The parameters are `lng`, `lat`, and `level=5` to give us all 5 administrative levels for france (Region, Department, Arrondissement (sous-préfecture), Commune, Arrondissement de commune (Paris/Lyon/Marseille)).

Sample response format :

```json
{
    "adminCode2": "38",
    "codes": [
        {
            "code": "ARA",
            "level": "1",
            "type": "ISO3166-2"
        },
        {
            "code": "38",
            "level": "2",
            "type": "ISO3166-2"
        }
    ],
    "adminCode3": "381",
    "adminName4": "Grenoble",
    "adminName3": "Grenoble",
    "adminCode1": "84",
    "adminName2": "Isère",
    "distance": 0,
    "countryCode": "FR",
    "countryName": "France",
    "adminName1": "Auvergne-Rhône-Alpes",
    "adminCode4": "38185"
}
```

- `http://api.geonames.org/findNearbyStreetsOSMJSON?lat=<lat>&lng=<lng>&username=<username>`

Sample response format : 

```json
{
    "streetSegment": [
        {
            "wayId": "26733966",
            "distance": "0.01",
            "line": "5.7203684 45.1797077,5.7204805 45.1796636,5.722798 45.1787519,5.7228994 45.178712,5.7233094 45.1785423,5.723574 45.1784773,5.7238582 45.1784737,5.7239993 45.1784719",
            "countryCode": "FR",
            "name": "Rue Le Brix",
            "maxspeed": "30",
            "highway": "residential"
        },
        {
            "wayId": "40197910",
            "distance": "0.04",
            "line": "5.7228994 45.178712,5.7229445 45.1787698,5.7231302 45.179008,5.7232132 45.1790825",
            "countryCode": "FR",
            "name": "Rue Saint-Exupéry",
            "maxspeed": "30",
            "highway": "residential"
        }
    ]
}
```

Geonames is free but has limitations on the rate of access. That's why you should use a username, linked to an account by them. You can use my account, the username is `quentin5478`.

# Steps to implementation

- Use the browser geolocation API to get the user coordinates when they clicks on the button
- Handle error case, display an error message if the coordinates could not be accessed
- Use AJAX to call the Geonames web services (you can use `fetch()`, `XMLHTTPRequest` or a third party ajax library such as axios)
- Again, handle the error cases when calling the web services, to display an error message if necessary
- From the response you got from the web services, construct a string message to display to the user indicating its current address : City name and street name

# Bonus feature 

Use the Geolocation API function `watchPosition()`, in order to have the position updated in real time,
and update the display on each location change.

You can then walk the streets of St Étienne with your smartphone and see if the street names updates as you walk.

(You will need to have your development server accessible while walking though)