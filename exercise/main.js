document.querySelector(".find-position-btn").addEventListener("click", () => {
    displayError("Nothing implemented yet!");
});



function displaySuccess(text) {
    _display(text, "success");
}

function displayError(text) {
    _display(text, "error");
}

function _display(text, status) {
    document.querySelectorAll(`.content .error`).forEach(e => e.remove());
    document.querySelectorAll(`.content .success`).forEach(e => e.remove());
    document.querySelector(".content").insertAdjacentHTML("beforeend", `<p class="${status}">${text}</p>`);
}